from vasara_manifest.lib import validate_toml
import jsonschema
import pytest


# Example TOML input
toml_example = """
# Your TOML example goes here
name = "John Doe"
age = 43
"""

# Invalid TOML input
toml_example_invalid = """
# Your TOML example goes here
name = "John Doe"
"""

# JSON schema for validation
json_schema = """
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "age": {
      "type": "integer"
    }
  },
  "required": ["name", "age"]
}
"""


def test_validate_toml() -> None:
    # Test case to validate TOML input against JSON schema
    validate_toml(toml_example, json_schema)


def test_validate_toml_invalid() -> None:
    # Test case to validate invalid TOML input against JSON schema
    with pytest.raises(jsonschema.exceptions.ValidationError):
        validate_toml(toml_example_invalid, json_schema)
