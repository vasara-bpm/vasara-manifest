from click.testing import CliRunner
from vasara_manifest.cli import validate


def test_validate_valid_input() -> None:
    runner = CliRunner()
    result = runner.invoke(validate, ["123"])
    assert result.exit_code == 0
    assert result.output.strip() == "123 is a valid input."


def test_validate_invalid_input() -> None:
    runner = CliRunner()
    result = runner.invoke(validate, ["abc"])
    assert result.exit_code == 0
    assert result.output.strip() == "abc is not a valid input."
