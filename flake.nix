{
  description = "Vasara Manifest";

  nixConfig = {
    extra-trusted-public-keys =
      "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4=";
    extra-substituters = "https://vasara-bpm.cachix.org";
  };

  inputs = {

    # Generic
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/master";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

    # Vasara
    vasara-bpm = {
      url = "gitlab:vasara-bpm/pkgs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

  };

  outputs = { self, nixpkgs, nixpkgs-unstable, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs-unstable = import nixpkgs-unstable {
          inherit system;
          overlays = [
            (self: super: {
              poetry = super.poetry.override { python3 = self.python311; };
            })
          ];
        };
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            inputs.poetry2nix.overlays.default
            (self: super: { inherit (inputs.gitignore.lib) gitignoreSource; })
          ];
        };
        overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: { });
        python =
          pkgs.python311.override { packageOverrides = self: super: { }; };
        call-name = "vasara-manifest";

      in {
        # Application
        apps.default = {
          type = "app";
          program = self.packages.${system}.default + "/bin/${call-name}";
        };

        # Python package
        packages.default = pkgs.poetry2nix.mkPoetryApplication {
          projectDir = pkgs.gitignoreSource ./.;
          preferWheels = true;
          inherit overrides python;
        };

        # Development environment
        packages.env = (pkgs.poetry2nix.mkPoetryEnv {
          projectDir = pkgs.gitignoreSource ./.;
          preferWheels = true;
          inherit overrides python;
        });

        # Development shell
        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs.entr
            pkgs.gnumake
            pkgs.jq
            pkgs-unstable.poetry
            self.packages.${system}.env
          ];
          shellHook = ''
            export PYTHONPATH=$(pwd)/src:$PYTHONPATH
          '';
        };

        devShells.with-podman =
          inputs.vasara-bpm.devShells.${system}.podman.overrideAttrs (old: {
            buildInputs = old.buildInputs
              ++ self.devShells.${system}.default.buildInputs;
          });

        formatter = pkgs.nixfmt;
      });
}
