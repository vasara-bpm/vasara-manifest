import json
import jsonschema
import toml


def validate_toml(toml_example: str, json_schema: str) -> None:
    # Parse the TOML example
    parsed_toml = toml.loads(toml_example)

    # Convert the JSON schema to a Python dictionary
    parsed_schema = json.loads(json_schema)

    # Validate the TOML example against the JSON schema
    jsonschema.validate(parsed_toml, parsed_schema)
