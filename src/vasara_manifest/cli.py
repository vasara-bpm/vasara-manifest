import click


@click.command()
@click.argument("input_value")
def validate(input_value: str) -> None:
    if input_value.isdigit():
        click.echo(f"{input_value} is a valid input.")
    else:
        click.echo(f"{input_value} is not a valid input.")


@click.group()
def cli() -> None:
    pass  # pragma: no cover


cli.add_command(validate)


def main() -> None:
    cli()  # pragma: no cover


if __name__ == "__main__":
    cli()  # pragma: no cover
