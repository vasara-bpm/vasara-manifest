SRCS := $(shell find src -name "*.py")

.PHONY: all
all: build

build: poetry.lock $(SRCS)
	$(RM) build
	nix build $(NIX_OPTIONS) -o build

.PHONY: clean
clean:
	rm -rf env result

.PHONY: coverage
coverage: htmlcov

.PHONY: format
format:
	black src tests
	isort src tests

.PHONY: shell
shell:
	nix develop $(NIX_OPTIONS)

.PHONY: check
check:
	black --check src tests
	isort -c src tests
	MYPYPATH=$(PWD)/stubs mypy --show-error-codes --strict src tests

.PHONY: watch
watch: watch_tests

.PHONY: watch_mypy
watch_mypy:
	find src tests -name "*.py"|MYPYPATH=$(PWD)/stubs entr mypy --show-error-codes --strict src tests

.PHONY: watch_pytest
watch_pytest:
	find src tests -name "*.py"|entr pytest tests

.PHONY: watch_tests
watch_tests:
	  $(MAKE) -j watch_mypy watch_pytest

.PHONY: pytest
pytest:
	pytest --cov=src --cov-report term --cov-report xml:.coverage.xml

.PHONY: test
test: check pytest

env: poetry.lock
	nix build .#env -o env

###

.PHONY: nix-%
nix-%:
	@echo "run inside nix devShell: $*"
	nix develop $(NIX_OPTIONS) --command $(MAKE) $*

.coverage: test

htmlcov: .coverage
	coverage html
